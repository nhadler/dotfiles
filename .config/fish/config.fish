alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

abbr --add cs config status
abbr --add cam config commit -am
abbr --add ccp config push

set fish_greeting

alias dsk="lsblk --output NAME,MOUNTPOINT,LABEL,FSTYPE,SIZE,RO,HOTPLUG,PARTTYPE,OWNER,GROUP,UUID"

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:/root/.local/bin:$PATH"

function mkcd -d "Create a directory and set CWD"
    command mkdir $argv
    if test $status = 0
        switch $argv[(count $argv)]
            case '-*'

            case '*'
                cd $argv[(count $argv)]
                return
        end
    end
end
