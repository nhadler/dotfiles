if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
set term=xterm-256color

call plug#begin('~/.vim/plugged')
Plug 'sirver/ultisnips'
Plug 'machakann/vim-sandwich'
Plug 'chaoren/vim-wordmotion'
Plug 'jalvesaq/Nvim-R'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-git'
Plug 'tpope/vim-surround'
Plug 'rhysd/vim-grammarous'
Plug 'lervag/vimtex'
Plug 'honza/vim-snippets'
Plug 'tpope/vim-fugitive'
Plug 'arcticicestudio/nord-vim'
Plug 'reedes/vim-pencil'
Plug 'kana/vim-textobj-user'
Plug 'reedes/vim-textobj-quote'
Plug 'easymotion/vim-easymotion'
Plug 'neomake/neomake'
Plug 'scrooloose/nerdtree'
Plug 'reedes/vim-textobj-sentence'
Plug 'reedes/vim-lexical'
Plug 'tmhedberg/SimpylFold'
Plug 'Konfekt/FastFold'
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
Plug 'tommcdo/vim-lion'
call plug#end()

" set statusline=%m\ %F\ %y\ %{&fileencoding?&fileencoding:&encoding}\ %=%(C:%c\ L:%l\ %P%)
set statusline=[%n]\ %<%.99f\ %h%w%m%r%{exists('*CapsLockStatusline')?CapsLockStatusline():''}%y%=%-16(\ %l,%c-%v\ %)%P
set termguicolors
set laststatus=2
set noshowmode
set number
"
" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

" neomake (linter) battery dependent settings
function! MyOnBattery()
  return readfile('/sys/class/power_supply/AC/online') == ['0']
endfunction
if MyOnBattery()
  call neomake#configure#automake('w')
else
  call neomake#configure#automake('nw', 1000)
endif

" color scheme settings 
colorscheme nord
" hi visual term=bold,reverse ctermfg=6 ctermbg=8 guifg=#88C0D0 guibg=#4C566A
" hi SpellBad term=bold,reverse ctermfg=5 ctermbg=11 guifg=#E5E9F0 guibg=#BF616A

" airline settings (status bar)
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" easy escape
" inoremap jj <Esc> 

" easy saving
:command WQ wq
:command Wq wq
:command W w
:command Q q

" easy motion keybinds
let g:EasyMotion_do_mapping = 0
nmap s <Plug>(easymotion-overwin-f2)
let g:EasyMotion_smartcase = 1
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

"copy goes to end of selection
vmap y ygv<Esc>

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" .tex file options
let g:vimtex_view_method = 'zathura'
let g:vimtex_compiler_latexmk = {'callback' : 0}
augroup textobj_quote
  autocmd!
  autocmd FileType tex call textobj#quote#init()
  autocmd FileType bib call textobj#quote#init()
augroup END
augroup pencil
  autocmd!
  autocmd FileType tex,bib call pencil#init({'wrap': 'soft'})
augroup END
augroup conceal
  autocmd!
  autocmd FileType tex,bib set conceallevel=0
augroup END
autocmd FileType tex :set spell
autocmd FileType tex :set lazyredraw
augroup VimCompletesMeTex
  autocmd!
    autocmd FileType tex
        \ let b:vcm_omni_pattern = g:vimtex#re#neocomplete
  augroup END
au FileType tex setlocal nocursorline
au FileType tex set number

" sentence objects
augroup textobj_sentence
  autocmd!
  autocmd FileType tex call textobj#sentence#init()
augroup END

" NerdTree toggle
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" enhanced dictionary/thesarus
augroup lexical
  autocmd!
  autocmd FileType tex call lexical#init()
  autocmd FileType text call lexical#init({ 'spell': 0 })
augroup END

" python folding settings
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab | 
    \ set autoindent |
    \ set fileformat=unix
let python_highlight_all=1

" ultisnips config
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

cmap w!! w !sudo tee %
nnoremap ; :
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u
